var serialPort = require('serialport'),
    events = require('events'),
	known_ports = [],
	ports = [],//useful ports
	ee = new events.EventEmitter;

setInterval(function() {
	serialPort.list(function (err, ports) {
		ports.forEach(function(new_port) {
			if (typeof(known_ports[new_port.comName]) === 'undefined') {
				ee.emit('new_serial_port', new_port.comName);
			}
		});
	});
}, 1000);

ee.on('new_serial_port', function(port_name) {
	var port = new serialPort.SerialPort(port_name, {'baudrate': 9600}, false);
	//console.log('Added port to known ports list: ' + port_name);
	known_ports[port_name] = port;

    known_ports[port_name].on('open', function() {
        var cmd = '';
        //console.log('open');
        //TODO: handshake here for id
        known_ports[port_name].write('{"event":"get_device_info"}\n', function(err, results) {
            //console.log(port_name + ': err ' + err);
            //console.log(port_name + ': results ' + results);
			console.log('Device plugged in on port: ' + port_name);
        });
        //TODO: handle multipart messages (see espruino code)
        known_ports[port_name].on('data', function(data) {
            cmd+=data;
            var idx = cmd.indexOf("\n");
            while (idx>=0) { 
                var line = cmd.substr(0,idx),
                    message = {};
                cmd = cmd.substr(idx+1);
                console.log('Data received from ' + port_name + ': ' + line);

                try {
                    message = JSON.parse(line);
                } catch (e) {
                    console.log('Error parsing response to JSON');
                }

                if (typeof(message.event) !== 'undefined') {
                    switch (message.event) {
                        case 'control_event': ee.emit('control_event', message.params); break;
                        case 'data_update_event': ee.emit('data_update_event', message.params); break;
                        case 'data_request_event': ee.emit('data_request_event', message.params); break;
                        case 'device_info_event': 
                            ports[message.params.id] = known_ports[port_name];
                            console.log(message.params.id + ' recognised on port: ' + port_name);
                            break;
                        default: 
                            if (typeof(message.params) !== 'undefined' && typeof(message.params.id) !== 'undefined') {
                                console.log('Unrecognised event received from serial device: ' + message.params.id);
                            } else {
                                console.log('Unrecognised event received from unknown serial device');
                            }
                    }
                } else {
                    console.log('Error: no event attached to message');
                }

                idx = cmd.indexOf("\n");
            }
        });
        known_ports[port_name].on('close', function() {
        	console.log('Device unplugged on port: ' + port_name);
        	known_ports[port_name] = undefined;
        	ports[port_name] = undefined;
        });
    });
	known_ports[port_name].open(function(error) {
		if (error) {
    		console.log("Serial: " + error);
    	}
	});
	//TODO: move adding port to array to handshake
});

var send = function(device_id, data) {
	console.log('Serial: Sending data to ' + device_id + ': ' + data);
    if (typeof(ports[device_id]) !== 'undefined') {
        ports[device_id].write(data, function(err, results) {
            console.log('err ' + err);
            console.log('results ' + results);
        });
    } else {
        console.log('Serial: Cannot send data to ' + device_id + ': ' + data);
    }
};

ee.on('send_data', function(type, device_id, data) {
	if (type === 'serial') {
        if (typeof(data) === 'object') data = JSON.stringify(data);
        if (data.indexOf('\n') !== data.length) {
            data = data + '\n';
        }
        log.debug('Serial: Sending data to ' + device_id + ': ' + data);
        if (typeof(ports[device_id]) !== 'undefined') {
            ports[device_id].write(data, function(err, results) {
                //console.log('err ' + err);
                //console.log('results ' + results);
            });
        } else {
            log.error('Serial: Cannot send data to ' + device_id + ': ' + data);
        }
    }
});